<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::get('auth/login',[\App\Http\Controllers\Auth\LoginController::class,'handleLogin']);
Route::get('auth/callback',[\App\Http\Controllers\Auth\LoginController::class,'handleCallback']);
Route::get('auth/user',[\App\Http\Controllers\Auth\LoginController::class,'getUserData']);

Route::group(['middleware' => 'auth:api'], function () {

});
