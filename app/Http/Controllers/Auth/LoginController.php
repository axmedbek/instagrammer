<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use GuzzleHttp\Client;

class LoginController extends Controller
{
    public function handleLogin(): string
    {
        $insta_url = 'https://api.instagram.com/oauth/authorize';
        $client = '2987077384893093';
        $uri = 'https://sociaman.com/api/auth/callback';
        $scope = 'user_profile';

        return redirect()->to($insta_url . '?client_id=' . $client . '&redirect_uri=' . $uri . '&response_type=code&scope=' . $scope);
    }

    public function handleCallback()
    {
        try {
            $client_id = '2987077384893093';
            $client_secret = '73fba29dcc6a5c39cfdd4ba3e5622ed4';
            $uri = 'https://sociaman.com/api/auth/callback';

            $client = new Client();
            $resp = $client->post('https://api.instagram.com/oauth/access_token', [
                'form_params' => [
                    'client_id' => $client_id,
                    'client_secret' => $client_secret,
                    'code' => request()->get('code'),
                    'grant_type' => 'authorization_code',
                    'redirect_uri' => $uri
                ]
            ]);

            $res = json_decode($resp->getBody()->getContents(), true);

            if (!is_null(@$res['access_token'])) {
                $longTokenClient = new Client();
                $longTokenResponse = $longTokenClient->get('https://graph.instagram.com/access_token', [
                    'query' => [
                        'grant_type' => 'ig_exchange_token',
                        'client_secret' => $client_secret,
                        'access_token' => $res['access_token']
                    ]
                ]);

                $longRes = json_decode($longTokenResponse->getBody()->getContents(), true);

                $userData = self::getUserData($longRes['access_token']);

                if ($userData['status']) {
                    dd(self::getUserDetail($userData['data']['username']));
                }
                else {
                    dd("Error happened");
                }
            }
        } catch (\Throwable $ex) {
            dd($ex->getMessage());
        }
    }

    function getUserData($access_token): array
    {
        try {
            $client = new Client();
            $userData = $client->get('https://graph.instagram.com/me', [
                'query' => [
                    'fields' => 'id,username,account_type',
                    'access_token' => $access_token,
                ]
            ]);

            return [
                'data' => json_decode($userData->getBody()->getContents(), true),
                'access_token' => $access_token,
                'status' => true
            ];
        } catch (\Throwable $exception) {
            return ['message' => $exception->getMessage(), 'status' => false];
        }
    }

    public function getUserDetail($username): array
    {
        $curl = curl_init();

        curl_setopt_array($curl, [
            CURLOPT_URL => "https://instagram85.p.rapidapi.com/account/" . $username . "/info",
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 30,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "GET",
            CURLOPT_HTTPHEADER => [
                "x-rapidapi-host: instagram85.p.rapidapi.com",
                "x-rapidapi-key: 86d4918ee4msh78434146275fb92p1312b3jsn8d79e021309a"
            ],
        ]);

        $response = curl_exec($curl);
        $err = curl_error($curl);

        curl_close($curl);

        if ($err) {
            return ['message' => $err, 'status' => false];
        } else {
            return ['data' => json_decode($response,true), 'status' => true];
        }
    }
}
